cmake_minimum_required(VERSION 2.6.0)

project(IR)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}")

find_package(OpenNI2 REQUIRED)

include_directories(${ZLIB_INCLUDE_DIR})
include_directories(${EIGEN_INCLUDE_DIRS})
include_directories(${OPENNI2_INCLUDE_DIR})

FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
LINK_DIRECTORIES(${OpenCV_LIB_DIR})
LINK_LIBRARIES(${OpenCV_LIBS})

file(GLOB srcs src/*.cpp)

set(CMAKE_CXX_FLAGS "-O3 -msse2 -msse3 -Wall -std=c++11")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -std=c++11")

add_definitions(-Dlinux=1)

add_executable(IR
               ${srcs}
)

target_link_libraries(IR
                      ${OPENNI2_LIBRARY}
)
